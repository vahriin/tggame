FROM python:latest
ADD *.py config.json /
RUN pip install aioredis
CMD ["python", "./main.py"]
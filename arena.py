from random import shuffle, choice
from typing import Dict, List, Union, Set

from bully import Bully
from actions import Action
from hit_case import HitCase
from action_result import ActResult


def form_result(
    bully: Bully, action_status: ActResult, target: str = "", hits: int = 0
) -> Dict[str, Union[str, int]]:
    res = {
        "nick": bully.nick,
        "action": int(bully.round_action),
        "action_status": int(action_status),
    }
    if not target == "":
        res.update({"target": target})
    if not hits == 0:
        res.update({"hits": hits})

    return res


class Arena:
    def __init__(self):
        self.bullies: Dict[str, Bully] = {}

    def _filter_action(self, action: Action) -> List[Bully]:
        result: List[Bully] = []
        for _, bully in self.bullies.items():
            if bully.round_action is action:
                result.append(bully)

        return result

    def add_bully(self, nick: str) -> None:
        self.bullies[nick] = Bully(nick)

    def delete_bully(self, nick: str) -> None:
        self.bullies.pop(nick)

    def set_action(self, nick: str, action: int, target: str = ""):
        if action == 2:
            self.add_bully(nick)
        elif action == 3:
            self.delete_bully(nick)
        else:
            try:
                if self.bullies.get(nick, None) is None:
                    self.add_bully(nick)
                self.bullies[nick].set_action(Action(action), target)
            except KeyError:
                print(f"Error! User {nick} not found")

    def _hit(self, hitman: Bully, target: Bully) -> Dict[str, Union[str, int]]:
        def evade(damage: int, act: ActResult = ActResult.HIT_SUCCESS):
            if not target.evasion:
                target.hitted = True
                killed = target.decrease_health(damage)
                if killed:
                    return form_result(hitman, ActResult.KILLED, target.nick, damage)
                return form_result(hitman, act, target.nick, damage)
            else:
                return form_result(hitman, ActResult.HIT_TARGET_EVADE, target.nick, 0)

        hit_status, damage = hitman.hit()
        if hit_status is HitCase.SLIP:
            return form_result(hitman, ActResult.HIT_SLIP, target.nick, 0)
        elif hit_status is HitCase.HIT:
            return evade(damage)
        elif hit_status is HitCase.HIT_BESIDE:
            others: List[Bully] = list(self.bullies.values())
            others.remove(hitman)
            try:
                others.remove(target)
            except ValueError:
                pass
            if len(others) == 0:
                return form_result(hitman, ActResult.HIT_SLIP, target.nick, 0)
            else:
                target = choice(others)
                return evade(damage, ActResult.HIT_BESIDE)
        raise Exception("WAT")

    def round(self):
        round_log: List[Dict[str, Union[str, int]]] = []
        try_hit: Set[str] = set()

        hitmans = self._filter_action(Action.HIT)
        shuffle(hitmans)
        for hitman in hitmans:
            if hitman.round_action is Action.KILLED:
                continue
            if hitman.target == "":
                bully_copy = list(self.bullies.values()).copy()
                bully_copy.remove(hitman)
                if len(bully_copy) == 0:
                    hitman.target = hitman.nick
                else:
                    hitman.target = choice(bully_copy).nick

            try_hit.add(hitman.target)
            round_log.append(self._hit(hitman, self.bullies[hitman.target]))

        for evader in self._filter_action(Action.EVASION):
            if evader.round_action is Action.KILLED:
                continue

            if not evader.hitted:
                evade_decrease = 5
                if evader.health > evade_decrease:
                    evader.decrease_health(evade_decrease)
                else:
                    evade_decrease = evader.health - 1
                    evader.decrease_health(evade_decrease)

                if evader.nick in try_hit:
                    round_log.append(
                        form_result(
                            evader, ActResult.EVADE_SUCCESS, hits=evade_decrease
                        )
                    )
                else:
                    round_log.append(
                        form_result(evader, ActResult.EVADE_FOOL, hits=evade_decrease)
                    )

        for stander in self._filter_action(Action.STAND):
            if stander.round_action is Action.KILLED:
                continue

            if not stander.hitted:
                stand_increase = 10
                stander.increase_health(stand_increase)
                round_log.append(
                    form_result(stander, ActResult.STAND_SUCCESS, hits=stand_increase)
                )

        for killed in self._filter_action(Action.KILLED):
            self.delete_bully(killed.nick)

        for bully in self.bullies.values():
            bully.round_end()

        return round_log

    def status(self):
        ret: Dict[str, int] = {}

        for bully in self.bullies.values():
            ret.update({bully.nick: bully.health})

        return ret

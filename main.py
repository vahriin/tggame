import asyncio
import json
from server import Server


async def main():
    print("Starting server")
    game_server = Server()
    with open("config.json") as conf:
        await game_server.async_init(**json.load(conf))
    print("Server started")

    try:
        await game_server.run()
    except KeyboardInterrupt:
        print("Stopping server")
        game_server.close()
        print("Server was stop")


if __name__ == "__main__":
    asyncio.run(main())

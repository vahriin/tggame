from enum import Enum


class Action(Enum):
    HIT = 0
    EVASION = 1
    NOT_ON_ARENA = 2
    KILLED = 3
    STAND = 4

    def __int__(self) -> int:
        return self.value

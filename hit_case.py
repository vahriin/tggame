from enum import Enum


class HitCase(Enum):
    HIT = 0
    HIT_BESIDE = 1
    SLIP = 2
